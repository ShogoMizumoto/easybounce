//
//  AppDelegate.m
//  EasyBounce
//
//  Created by ShogoMizumoto on 2014/07/01.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//

#import "AppDelegate.h"
#import "BounceViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];

    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[BounceViewController new]];
    [navigationController setNavigationBarHidden:YES];
    self.window.rootViewController = navigationController;

    [self.window makeKeyAndVisible];
    return YES;
}

@end