//
//  main.m
//  EasyBounce
//
//  Created by ShogoMizumoto on 2014/07/01.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }

}