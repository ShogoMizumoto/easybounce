//
//  BounceViewController.m
//  EasyBounce
//
//  Created by ShogoMizumoto on 2014/07/01.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//

#import "BounceViewController.h"
#import "BounceItem.h"
#import "BounceViewCell.h"

static NSString *const kTableCell_Identifier = @"BounceViewCell";
static NSString *const kTableCell_NibName    = @"BounceViewCell";

@implementation BounceViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.automaticallyAdjustsScrollViewInsets = NO;

    [self.uTableBounceView registerNib:[UINib nibWithNibName:kTableCell_NibName bundle:nil] forCellReuseIdentifier:kTableCell_Identifier];

    [self initDataSource];
}

- (void)initDataSource
{
    self.mBounceItemArray = [NSMutableArray array];

    [self.mBounceItemArray addObject:[[BounceItem alloc] initWithPoint_X1:0.1f point_Y1:1.0f point_X2:0.2f point_Y2:1.1f]];
    [self.mBounceItemArray addObject:[[BounceItem alloc] initWithPoint_X1:0.2f point_Y1:1.0f point_X2:0.3f point_Y2:1.1f]];
    [self.mBounceItemArray addObject:[[BounceItem alloc] initWithPoint_X1:0.3f point_Y1:1.0f point_X2:0.4f point_Y2:1.1f]];
    [self.mBounceItemArray addObject:[[BounceItem alloc] initWithPoint_X1:0.4f point_Y1:1.0f point_X2:0.5f point_Y2:1.1f]];
    [self.mBounceItemArray addObject:[[BounceItem alloc] initWithPoint_X1:0.5f point_Y1:1.0f point_X2:0.6f point_Y2:1.1f]];
    [self.mBounceItemArray addObject:[[BounceItem alloc] initWithPoint_X1:0.6f point_Y1:1.0f point_X2:0.7f point_Y2:1.1f]];
    [self.mBounceItemArray addObject:[[BounceItem alloc] initWithPoint_X1:0.7f point_Y1:1.0f point_X2:0.8f point_Y2:1.1f]];
    [self.mBounceItemArray addObject:[[BounceItem alloc] initWithPoint_X1:0.8f point_Y1:1.0f point_X2:0.9f point_Y2:1.1f]];
    [self.mBounceItemArray addObject:[[BounceItem alloc] initWithPoint_X1:0.9f point_Y1:1.0f point_X2:1.0f point_Y2:1.1f]];

    [self.mBounceItemArray addObject:[[BounceItem alloc] initWithPoint_X1:0.9f point_Y1:-0.1f point_X2:1.0f point_Y2:-0.2f]];
    [self.mBounceItemArray addObject:[[BounceItem alloc] initWithPoint_X1:0.8f point_Y1:-0.1f point_X2:0.9f point_Y2:-0.2f]];
    [self.mBounceItemArray addObject:[[BounceItem alloc] initWithPoint_X1:0.7f point_Y1:-0.1f point_X2:0.8f point_Y2:-0.2f]];
    [self.mBounceItemArray addObject:[[BounceItem alloc] initWithPoint_X1:0.6f point_Y1:-0.1f point_X2:0.7f point_Y2:-0.2f]];
    [self.mBounceItemArray addObject:[[BounceItem alloc] initWithPoint_X1:0.5f point_Y1:-0.1f point_X2:0.6f point_Y2:-0.2f]];
    [self.mBounceItemArray addObject:[[BounceItem alloc] initWithPoint_X1:0.4f point_Y1:-0.1f point_X2:0.5f point_Y2:-0.2f]];
    [self.mBounceItemArray addObject:[[BounceItem alloc] initWithPoint_X1:0.3f point_Y1:-0.1f point_X2:0.4f point_Y2:-0.2f]];
    [self.mBounceItemArray addObject:[[BounceItem alloc] initWithPoint_X1:0.2f point_Y1:-0.1f point_X2:0.3f point_Y2:-0.2f]];
    [self.mBounceItemArray addObject:[[BounceItem alloc] initWithPoint_X1:0.1f point_Y1:-0.1f point_X2:0.2f point_Y2:-0.2f]];
}




#pragma mark - Delegate : UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.mBounceItemArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BounceItem     *item = [self.mBounceItemArray objectAtIndex:indexPath.row];
    BounceViewCell *cell = (BounceViewCell *) [tableView dequeueReusableCellWithIdentifier:kTableCell_Identifier];

    if (cell == nil) {
        UINib   *nib   = [UINib nibWithNibName:kTableCell_Identifier bundle:nil];
        NSArray *array = [nib instantiateWithOwner:self options:nil];

        cell = [array objectAtIndex:0];
    }

    return [cell updateCell:item];
}

@end
