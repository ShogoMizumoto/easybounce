//
//  BounceItem.h
//  EasyBounce
//
//  Created by ShogoMizumoto on 2014/07/02.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//

@interface BounceItem : NSObject

@property CGFloat mTimingPoint_X1;
@property CGFloat mTimingPoint_Y1;
@property CGFloat mTimingPoint_X2;
@property CGFloat mTimingPoint_Y2;

- (id)initWithPoint_X1:(CGFloat)point_X1
              point_Y1:(CGFloat)point_Y1
              point_X2:(CGFloat)point_X2
              point_Y2:(CGFloat)point_Y2;

@end
