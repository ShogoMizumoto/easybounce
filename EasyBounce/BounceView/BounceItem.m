//
//  BounceItem.m
//  EasyBounce
//
//  Created by ShogoMizumoto on 2014/07/02.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//

#import "BounceItem.h"

@implementation BounceItem

- (id)initWithPoint_X1:(CGFloat)point_X1
              point_Y1:(CGFloat)point_Y1
              point_X2:(CGFloat)point_X2
              point_Y2:(CGFloat)point_Y2
{
    self = [super init];
    if (self) {
        self.mTimingPoint_X1 = point_X1;
        self.mTimingPoint_Y1 = point_Y1;
        self.mTimingPoint_X2 = point_X2;
        self.mTimingPoint_Y2 = point_Y2;
    }
    return self;
}

@end
