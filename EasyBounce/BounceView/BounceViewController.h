//
//  BounceViewController.h
//  EasyBounce
//
//  Created by ShogoMizumoto on 2014/07/01.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//



@interface BounceViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *uTableBounceView;

@property NSMutableArray *mBounceItemArray;

@end
