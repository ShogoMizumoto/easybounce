//
//  BounceViewCell.m
//  EasyBounce
//
//  Created by ShogoMizumoto on 2014/07/02.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//

#import "BounceViewCell.h"

static NSString *const kLabelFormat_TimingPoint1 = @"Point - X1:%f Y1:%f";
static NSString *const kLabelFormat_TimingPoint2 = @"Point - X2:%f Y2:%f";

static NSString *const kAnimKey_MoveRight = @"MoveRight";
static NSString *const kAnimKey_MoveLeft  = @"MoveLeft";

static CGPoint const kAnimationCircleLocation_Right = {250.0f, 60.0f};
static CGPoint const kAnimationCircleLocation_Left  = { 20.0f, 60.0f};

@interface BounceViewCell ()
{
    BOOL isAnimating;
}
@end

@implementation BounceViewCell

- (id)updateCell:(BounceItem *)item
{
    self.mItem = item;

    [self initView];

    return self;
}

- (void)initView
{
    self.uLabelTimingPoint1.text = [[NSString alloc] initWithFormat:kLabelFormat_TimingPoint1,
                                                                    self.mItem.mTimingPoint_X1,
                                                                    self.mItem.mTimingPoint_Y1];

    self.uLabelTimingPoint2.text = [[NSString alloc] initWithFormat:kLabelFormat_TimingPoint2,
                                                                    self.mItem.mTimingPoint_X2,
                                                                    self.mItem.mTimingPoint_Y2];

    [self.uImageCircle.layer removeAllAnimations];

    self.uImageCircle.frame = CGRectMake(
            kAnimationCircleLocation_Left.x,
            kAnimationCircleLocation_Left.y,
            self.uImageCircle.frame.size.width,
            self.uImageCircle.frame.size.height);

    isAnimating = NO;
}

- (IBAction)tapButtonAnimationTrigger:(id)sender
{
    [self startAnimationCircleImage];
}






#pragma mark - Animation CircleImage

- (void)startAnimationCircleImage
{
    if (isAnimating) return;

    isAnimating = YES;

    BOOL isMoveRight = self.uImageCircle.frame.origin.x < self.center.x;

    [self setAnimationProperty:isMoveRight];
}

- (void)setAnimationProperty:(BOOL)isMoveRight
{
    CABasicAnimation *anim   = [CABasicAnimation animationWithKeyPath:@"position"];
    anim.duration            = 0.75f;
    anim.delegate            = self;
    anim.repeatCount         = 0;
    anim.autoreverses        = NO;
    anim.removedOnCompletion = NO;
    anim.fillMode            = kCAFillModeForwards;
    anim.timingFunction      = [self timingFunction];

    CGPoint from = isMoveRight ? kAnimationCircleLocation_Left  : kAnimationCircleLocation_Right;
    CGPoint to   = isMoveRight ? kAnimationCircleLocation_Right : kAnimationCircleLocation_Left;

    anim.fromValue = [NSValue valueWithCGPoint:CGPointMake(
            from.x + self.uImageCircle.frame.size.width  / 2,
            from.y + self.uImageCircle.frame.size.height / 2)];

    anim.toValue   = [NSValue valueWithCGPoint:CGPointMake(
            to.x + self.uImageCircle.frame.size.width  / 2,
            to.y + self.uImageCircle.frame.size.height / 2)];

    [self.uImageCircle.layer addAnimation:anim forKey: ((isMoveRight) ? kAnimKey_MoveRight : kAnimKey_MoveLeft) ];
}

- (CAMediaTimingFunction *)timingFunction
{
    return [CAMediaTimingFunction functionWithControlPoints
    :self.mItem.mTimingPoint_X1 :self.mItem.mTimingPoint_Y1 // ポイント1の時間と効果の値
    :self.mItem.mTimingPoint_X2 :self.mItem.mTimingPoint_Y2 // ポイント2の時間と効果の値
    ];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    BOOL isMoveRight = self.uImageCircle.frame.origin.x < self.center.x;

    CGPoint newPoint;

    if (isMoveRight) {
        newPoint = kAnimationCircleLocation_Right;
    } else {
        newPoint = kAnimationCircleLocation_Left;
    }

    self.uImageCircle.frame = CGRectMake(
            newPoint.x,
            newPoint.y,
            self.uImageCircle.frame.size.width,
            self.uImageCircle.frame.size.height);

    isAnimating = NO;
}

@end
