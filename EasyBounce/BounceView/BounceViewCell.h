//
//  BounceViewCell.h
//  EasyBounce
//
//  Created by ShogoMizumoto on 2014/07/02.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//

#import "BounceItem.h"

@interface BounceViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel     *uLabelTimingPoint1;
@property (weak, nonatomic) IBOutlet UILabel     *uLabelTimingPoint2;
@property (weak, nonatomic) IBOutlet UIImageView *uImageCircle;

@property BounceItem *mItem;

- (id)updateCell:(BounceItem *)item;
- (void)startAnimationCircleImage;

- (IBAction)tapButtonAnimationTrigger:(id)sender;

@end
